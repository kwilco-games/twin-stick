# twin-stick

A twin stick shooter game. Mostly to learn pixi.js and the Gamepad API.

## [PLAY IT HERE](http://kwilco.gitlab.io/twin-stick/)

Stuff I'm using that other people made:
* https://opengameart.org/content/animated-spaceships
* https://opengameart.org/content/bullet-collection-1-m484
* http://www.pixijs.com/