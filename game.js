/* global PIXI */
"use strict";


var MAX_BULLETS = 5000;
var SCREEN_SIZE = 500;  // in pixels
var SCREEN_RECT = {"x": 0, "y": 0, "width": SCREEN_SIZE, "height": SCREEN_SIZE};
var DEADZONE = 0.1;  // Gamepad stick values below this are ignored.

var state = {};  // game state


function intro() {
    var intro_message = "WebGL support activated!";
    if(!PIXI.utils.isWebGLSupported()){
        intro_message = "Falling back to canvas.";
    }
    PIXI.utils.sayHello(intro_message);
}
intro();


var renderer = PIXI.autoDetectRenderer(SCREEN_SIZE, SCREEN_SIZE);
document.getElementById("game").appendChild(renderer.view);


PIXI.loader
    .add("img/Ships/Turtle.png")
    .add("img/bullet.png")
    .load(setup);

function setup() {
    var i, bullet;

    state["containers"] = {};

    state.containers["scene"] = new PIXI.Container();
    state.containers["bullets"] = new PIXI.Container();

    state["player"] = new PIXI.Sprite(
        PIXI.loader.resources["img/Ships/Turtle.png"].texture
    );
    state.player.x = SCREEN_SIZE / 2;
    state.player.y = SCREEN_SIZE / 2;
    state["bullets"] = [];

    for (i = 0; i < MAX_BULLETS; i++) {
        bullet = new PIXI.Sprite(
            PIXI.loader.resources["img/bullet.png"].texture
        );
        
        bullet.x = 0;
        bullet.y = 0;
        bullet.vx = 0;
        bullet.vy = 0;
        bullet.visible = false;

        state.bullets.push(bullet);
        state.containers.bullets.addChild(bullet);
    }

    state.containers.scene.addChild(state.player);
    state.containers.scene.addChild(state.containers.bullets);

    main();
}


function deadzone(value) {
    return Math.abs(value) > DEADZONE;
}

function update() {
    var pad;
    var i, j, bullet;

    pad = niceGamepad(navigator.getGamepads()[0]);

    if (deadzone(pad.ls.x)) {
        state.player.x += 5.0 * pad.ls.x;
    }
    if (deadzone(pad.ls.y)) {
        state.player.y += 5.0 * pad.ls.y;
    }

    if (deadzone(pad.rs.x) || deadzone(pad.rs.y)) {

        for (i = 0; i < MAX_BULLETS; i++) {
            bullet = state.bullets[i];

            // find first invisible (dead) bullet. for performance?
            if (!state.bullets[i].visible) {
                bullet.visible = true;
                bullet.x = state.player.x + state.player.width / 4;  // why by 4 and not 2?? D:
                bullet.y = state.player.y + state.player.height / 4;
                bullet.vx = 10.0 * pad.rs.x;
                bullet.vy = 10.0 * pad.rs.y;

                break;
            }
        }
    }

    if (pad.a.pressed) {
        for (j = 0; j < 10; j ++) {
            for (i = 0; i < MAX_BULLETS; i++) {
                bullet = state.bullets[i];

                // find first invisible (dead) bullet. for performance?
                if (!state.bullets[i].visible) {
                    bullet.visible = true;
                    bullet.x = Math.random() * SCREEN_SIZE;
                    bullet.y = Math.random() * SCREEN_SIZE;
                    bullet.vx = 20.0 * Math.random() - 10.0;
                    bullet.vy = 20.0 * Math.random() - 10.0;

                    break;
                }
            }
        }
    }

    for (i = 0; i < MAX_BULLETS; i++) {
        bullet = state.bullets[i];
        
        // only update visible (alive) bullets
        if (bullet.visible) {
            bullet.x += bullet.vx;
            bullet.y += bullet.vy;

            if (!collide(bullet, SCREEN_RECT)) {
                bullet.visible = false;
            }
        }
    }
}


function draw() {
    renderer.render(state.containers.scene);
}


function main() {
    gameLoop();
}


function gameLoop() {
    update();
    draw();
    requestAnimationFrame(gameLoop);
}


// return true if rectangles collide
// copied and modified from https://github.com/kittykatattack/learningPixi#collision
function collide(r1, r2) {

    //Define the variables we'll need to calculate
    var combinedHalfWidths, combinedHalfHeights, vx, vy;

    //Find the center points of each sprite
    r1.centerX = r1.x + r1.width / 2;
    r1.centerY = r1.y + r1.height / 2;
    r2.centerX = r2.x + r2.width / 2;
    r2.centerY = r2.y + r2.height / 2;

    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    //Calculate the distance vector between the sprites
    vx = r1.centerX - r2.centerX;
    vy = r1.centerY - r2.centerY;

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    //Check for a collision on the x axis
    return (
        (Math.abs(vx) < combinedHalfWidths)
        && (Math.abs(vy) < combinedHalfHeights)
    );
}

// returns a human-readable gamepad object
function niceGamepad(gamepad) {
    var buttons, axes;
    var ls_x, ls_y, rs_x, rs_y;

    // simulate a neutral controller if undefined gets passed in (aka no controller present)
    buttons = gamepad ? gamepad.buttons : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    axes = gamepad ? gamepad.axes : [0.0, 0.0, 0.0, 0.0];

    // make damn sure these don't change between now and the trig functions
    ls_x = axes[0];
    ls_y = axes[1];
    rs_x = axes[2];
    rs_y = axes[3];

    return {
        a: buttons[0],
        b: buttons[1],
        x: buttons[2],
        y: buttons[3],
        l1: buttons[4],
        r1: buttons[5],
        l2: buttons[6],
        r2: buttons[7],
        select: buttons[8],
        start: buttons[9],
        l3: buttons[10],
        r3: buttons[11],
        up: buttons[12],
        down: buttons[13],
        left: buttons[14],
        right: buttons[15],
        ls: {
            x: ls_x,
            y: ls_y,
            angle: Math.atan2(ls_y, ls_x),
            magnitude: Math.sqrt(ls_x * ls_x + ls_y * ls_y)
        },
        rs: {
            x: rs_x,
            y: rs_y,
            angle: Math.atan2(rs_y, rs_x),
            magnitude: Math.sqrt(rs_x * rs_x + rs_y * rs_y)
        }
    };
}